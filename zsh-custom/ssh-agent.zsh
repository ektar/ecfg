[[ ! -z "$SSH_AUTH_SOCK" ]] && ssh-add -l 1>/dev/null 2>&1
if [ $? -eq 0 ]; then
    >&2 echo Reusiing ssh agent at $SSH_AUTH_SOCK
else
    mkdir -p ~/.ssh/agent-sockets
    export SSH_AUTH_SOCK=~/.ssh/agent-sockets/ssh_auth_sock.$(hostname)

    ssh-add -l 2>/dev/null >/dev/null
    if [ $? -ge 2 ]; then
        >&2 echo Creating new ssh agent at $SSH_AUTH_SOCK

        ssh-agent -a "$SSH_AUTH_SOCK" >/dev/null
    else
        >&2 echo Reusing ssh agent at $SSH_AUTH_SOCK
    fi
fi
